var responseStatus = 0;
var counter = 0;
var review = {};

function loadDoc(url, cFunction) {
  
  var xhttp;
  xhttp=new XMLHttpRequest();

  xhttp.onreadystatechange = function() {
    
    if (this.readyState == 4 && this.status == 200) {
    cFunction(this);
      responseStatus = 200;
        }else if(this.readyState == 2 && this.status == 500){
          responseStatus =500;
                
              }
              else if(this.readyState == 4 && this.status == 500){
                cFunction(this);
                responseStatus =500;
                
              }
            };
  xhttp.open("GET", url, true);
        if(responseStatus !=500){
        
        xhttp.send();
      }
        return responseStatus;
      }

//on "review button" this function changes buttons text and opens hidden div
function changeText(param){
    
    var buttonReview = document.getElementById("buttonReview".concat(param))
    
    if (buttonReview.innerHTML == "Show reviews")
                {
                    buttonReview.innerHTML = "Hide reviews";
                    document.getElementById("hidden_".concat(param)).setAttribute("style", "position:static;background-color:rgba(163,163,163,1);  width:100%; height:295px;   margin: -15px 15px 15px 15px;border-radius:0px 0px 5px 5px; border:solid 1px gray");
                    document.getElementById("box_".concat(param)).setAttribute("style", "position:static; margin:15px; width:100%; height:295px; border: 1px solid black; display:inline-flex;border-radius:5px 5px 0px 0px");
                    
                }
                else if (buttonReview.innerHTML == "Hide reviews") {
                    buttonReview.innerHTML = "Show reviews";
                    document.getElementById("hidden_".concat(param)).setAttribute("style", "position:static;width:100%;height:315px;border:2px solid gray; display:none;margin: -15px 15px 15px 15px;border-radius:0px 0px 5px 5px");
                    document.getElementById("box_".concat(param)).setAttribute("style", "position:static;margin:15px;width:100%;height:295px;border:1px solid black;display:inline-flex;border-radius:5px 5px 5px 5px");
                }
}


//this function sends another HttpRequest using also hotel_id as paramA and hotel number as paramB
function showReviews (paramA,paramB){
  
  var xhttp;
  xhttp=new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    review = JSON.parse(xhttp.responseText);
     
    

    for(i=0;i<review.length;i++){
          if(review.length > 0){
                for(x=0;x<review.length;x++){
                  
                    if(review[x].positive == true ){
                        
                        document.getElementById("positiveCommentTitle".concat(paramB)).innerHTML= review[x].name;
                        document.getElementById("positiveCommentParagraph_".concat(paramB)).innerHTML= review[x].comment;
                        
                        }else if (review[x].positive == false ){
                    
                        document.getElementById("negativeCommentTitle_".concat(paramB)).innerHTML= review[x].name;
                        document.getElementById("negativeCommentParagraph_".concat(paramB)).innerHTML= review[x].comment;
                            
                            } 
                          }
                        }
                      }
                    }else if(this.readyState == 2 && this.status == 500){
                            
                            return review = JSON.parse(xhttp.responseText);
                            
                  }
                          else if(this.readyState == 4 && this.status == 500){
                           return review = JSON.parse(xhttp.responseText);
                            
                }
              };
  xhttp.open("GET", 'http://fake-hotel-api.herokuapp.com/api/reviews?hotel_id='+paramA, true);
  xhttp.send();
  
  }

//this function generates HTML content
function myFunction(xhttp) {
//counter check if "Load Hotels" buttons is clicked. This prevents content to be load again if it is already loaded
counter ++;
if(responseStatus == 500 && counter == 1){

  var errorDiv = document.createElement("div");
        //error div
        errorDiv.class = "error_div";
        errorDiv.id = "error_div";
        
        errorDiv.setAttribute("style", "position:static; margin:15px; width:100%; height:130%; border: 1px solid black;background-color: rgb(213,210,210); display:inline-flex;border-radius:5px;");
        var erp= document.createElement("H1");
        erp.id = "erpID";
      
        erp.setAttribute("style","padding-left: 10px; font-size: 100%; margin:auto; line-height:51px; vertical-align:middle; font-family:Open Sans Regular; ")
        erp.innerHTML = "An error occured";
       

       document.getElementById('demo').appendChild(errorDiv);
       document.getElementById("error_div").appendChild(erp);
       //on timeout relaod again
       setTimeout(function(){
        location.reload();
       },3000);
}

 else if(responseStatus = 200 && counter == 1) {
 //parsing responseText from server into JSON Object
 var obj = JSON.parse(xhttp.responseText);
 
  //iteration through responseText object 
  for(var i=0; i<obj.length;i++) {
    //main content div
  	var newDiv = document.createElement("div");
  	
  	
      newDiv.class = "box";
      newDiv.id = "box_".concat(i);
    	document.getElementById('demo').appendChild(newDiv);
      newDiv.setAttribute("style", "position:static; margin:15px; width:100%; height:295px; border: 1px solid black; display:inline-flex;border-radius:5px;");
  	
    //review div, hidden until click "Show reviews"
    var hiddenDiv = document.createElement("div");
    
    hiddenDiv.id= "hidden_".concat(i);
    hiddenDiv.setAttribute("style", "position:static; background-color:rgba(163,163,163,0.7); width:100%; height:295px; border: 2px solid red; display:none; margin: -15px 15px 15px 15px;border-radius:0px 0px 5px 5px;");  
    document.getElementById('demo').appendChild(hiddenDiv);
    
    //calling function which loads hotel reviews
    showReviews(obj[i].id,i);

    var positive_comment = document.createElement("div");
      positive_comment.id = "positive_comment_".concat(i);  
      positive_comment.setAttribute("style","width:98%;height:30%;margin-left:1%");
      

    document.getElementById("hidden_".concat(i)).appendChild(positive_comment);

    var positive_circle = document.createElement("div");
      positive_circle.id = "plusSign";
      positive_circle.setAttribute("style","margin-top:5%; margin-left:5%; border-radius:999px;width:47px;height:47px;background:rgb(213,219,219);color: #003580;text-align:center;font:25px Arial,sans-serif;line-height: 1.95em;");
      positive_circle.innerHTML= '&#43';

    document.getElementById("positive_comment_".concat(i)).appendChild(positive_circle);  

     var positiveCommentTitle = document.createElement('H1');
      positiveCommentTitle.id = "positiveCommentTitle".concat(i);
      positiveCommentTitle.setAttribute("style","margin-top:-7%;margin-left:15%;font-size:95%;font-family:Open Sans Regular;text-transform:capitalize");
      positiveCommentTitle.innerHTML = "There is no positive comment for this Hotel.";

    document.getElementById("positive_comment_".concat(i)).appendChild(positiveCommentTitle);
      
      var positiveCommentParagraph = document.createElement('p');
      positiveCommentParagraph.id = "positiveCommentParagraph_".concat(i);
      positiveCommentParagraph.setAttribute("style","width:84%;margin-left:15%;font-size:75%;line-height:100%;font-family:Open Sans Regular;text-transform:capitalize; word-break:break-all");
      

    document.getElementById("positive_comment_".concat(i)).appendChild(positiveCommentParagraph);



    var negative_comment = document.createElement("div");
      negative_comment.id = "negative_comment_".concat(i);  
      negative_comment.setAttribute("style","width:98%;height:50%;margin-left:1%;border-top:solid 2px gray");
      

    document.getElementById("hidden_".concat(i)).appendChild(negative_comment);

    var negative_circle = document.createElement("div");
      var minus_id = "minus_";
      negative_circle.id = "minusSign";
      negative_circle.setAttribute("style","border-radius:999px;margin-top:4%; margin-left:5%; width:47px;height:47px;background:rgb(213,219,219); color: #003580;text-align:center;font:25px Arial,sans-serif;line-height: 1.8em;");
      negative_circle.innerHTML= '&#8211' ;

    document.getElementById("negative_comment_".concat(i)).appendChild(negative_circle);

    var negativeCommentTitle = document.createElement('H1');
      negativeCommentTitle.id = "negativeCommentTitle_".concat(i);
      negativeCommentTitle.setAttribute("style","margin-top:-7%;margin-left:15%;font-size:95%;font-family:Open Sans Regular;text-transform:capitalize");
      negativeCommentTitle.innerHTML = "There is no negative comment for this Hotel.";
    document.getElementById("negative_comment_".concat(i)).appendChild(negativeCommentTitle); 

     var negativeCommentParagraph = document.createElement('p');
      negativeCommentParagraph.id = "negativeCommentParagraph_".concat(i);
      negativeCommentParagraph.setAttribute("style","margin-left:15%;width:84%;font-size:75%;line-height:100%;font-family:Open Sans Regular;text-transform:capitalize; word-break:break-all");
      

    document.getElementById("negative_comment_".concat(i)).appendChild(negativeCommentParagraph);




  	// Hotel image
    var pictureSRC = document.createElement("IMG");
    	
      pictureSRC.setAttribute("src",obj[i].images[0]);
    	pictureSRC.setAttribute("style", "width:40%; height:100%; background-color: rgb(213,210,210);");
      pictureSRC.setAttribute("alt", "Image not loaded");
    	document.getElementById("box_".concat(i)).appendChild(pictureSRC);
    
    //infoBox 
    var info_box = document.createElement("DIV");
      
      info_box.class="info_box";
      info_box.id= "info_box".concat(i);
      info_box.setAttribute("style","width:60%;background-color: rgb(243,240,240);");
    
    document.getElementById("box_".concat(i)).appendChild(info_box);
    
    //headLine
    var info_text_up = document.createElement("DIV");
    
    info_text_up.id="info_text_up".concat(i);
    info_text_up.setAttribute("style","width:100%; height:50%; ");
      
    var h1 = document.createElement("H1");
      h1.setAttribute("style"," width:70%; padding-left: 3%; font-size: 130%; font-family:Open Sans Regular; text-transform: capitalize;");
      
    
      h1.id="h1".concat(i);
      h1.innerHTML = obj[i].name;
    document.getElementById("info_box".concat(i)).appendChild(info_text_up);


    var stars = document.createElement('p');
      stars.id = "stars_".concat(i);
      stars.setAttribute("style","width:40%;margin-top:0%;margin-right:3%;font-size:120%;text-align:right;float:right");
      // stars.innerHTML = "&#x2605; &#x2606; &#x2606; &#x2606; &#x2606;";
    switch(obj[i].stars){
      case 1:
      stars.innerHTML = "&#x2605; &#x2606; &#x2606; &#x2606; &#x2606;";
      break;
      case 2:
      stars.innerHTML = "&#x2605; &#x2605; &#x2606; &#x2606; &#x2606;";
      break;
      case 3:
      stars.innerHTML = "&#x2605; &#x2605; &#x2605; &#x2606; &#x2606;";
      break;
      case 4:
      stars.innerHTML = "&#x2605; &#x2605; &#x2605; &#x2605; &#x2606;";
      break;
      case 5:
      stars.innerHTML = "&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;";
      break;
      default:
      stars.innerHTML = "&#x2606; &#x2606; &#x2606; &#x2606; &#x2606;";

    }


    document.getElementById("info_text_up".concat(i)).appendChild(stars);  

      
      var parag= document.createElement("p");
      
      parag.setAttribute("style","margin-top:-3%;width:100%;padding-left: 3%; font-size:80%; font-family:Open Sans Regular; text-transform: capitalize;")
      parag.innerHTML=obj[i].country;
      parag.id = "p".concat(i);
    

    document.getElementById("info_text_up".concat(i)).appendChild(h1); 
    document.getElementById("info_text_up".concat(i)).appendChild(parag)

    var parag2= document.createElement("p");
     
      parag2.setAttribute("style","margin-top:4%;width:95%;padding-left: 3%; line-height: 100%;font-size:small;word-break: break-all;font-family:Open Sans Semibold; text-transform: capitalize;")
      parag2.innerHTML=obj[i].description;
      
      parag2.id = "pt".concat(i);
    
    document.getElementById("info_text_up".concat(i)).appendChild(parag2) 

    var info_text_bottom = document.createElement("DIV");
     info_text_bottom.id = "info_text_bottom".concat(i);
     info_text_bottom.setAttribute("style","width:100%; height:43%; ");
 
    document.getElementById("info_box".concat(i)).appendChild(info_text_bottom);

    var buttonReview = document.createElement("button");
      
      
      buttonReview.id = "buttonReview".concat(i);
      buttonReview.innerHTML = "Show reviews";
          

      buttonReview.setAttribute("style","background-color: rgb(213,219,219);width:33%; height:39%; margin-top:7%; margin-left:3%; font-weight:600; border-radius:5px; border:solid 1px black");
      buttonReview.setAttribute("onclick",'changeText('+i+')')

    document.getElementById("info_text_bottom".concat(i)).appendChild(buttonReview);



    var price_box = document.createElement("DIV");
      price_box.id= "price_box_".concat(i);
      price_box.setAttribute("style","width:45%;height:45%; margin-top:7%; float:right;");

      document.getElementById("info_text_bottom".concat(i)).appendChild(price_box);

    var price_h = document.createElement("H1");
       
       price_h.id = "price_h_".concat(i);
       price_h.setAttribute("style","width:50%; float:right;margin-top:-4%; margin-right:7%;text-align:right;font-family:Open Sans Regular;");
       price_h.innerHTML= obj[i].price +' €';

      document.getElementById("price_box_".concat(i)).appendChild(price_h);    

    var parag_time= document.createElement("p");
      parag_time.id = "time_parag_".concat(i);
      parag_time.setAttribute("style","width: 132%;margin-top:-8%;;margin-right:7%; font-size:initial;text-align:right; float:right;");
            
            var start_date = new Date(obj[i].date_start);
                var year = start_date .getFullYear();
                var month = start_date .getMonth()+1;
                var dt = start_date .getDate();

                  if (dt < 10) {
                    dt = '0' + dt;
                  }
                      if (month < 10) {
                        month = '0' + month;
                      }
              start_date = dt+'.'+month+'.'+year;
                      
                      var end_date = new Date(obj[i].date_end);
                          var year = end_date .getFullYear();
                          var month = end_date .getMonth()+1;
                          var dt = end_date .getDate();

                            if (dt < 10) {
                              dt = '0' + dt;
                            }
                                if (month < 10) {
                                  month = '0' + month;
                                }
                                  end_date = dt+'.'+month+'.'+year;

      parag_time.innerHTML = start_date+' - '+end_date;

       document.getElementById("price_box_".concat(i)).appendChild(parag_time); 

}
}
else if (counter != 1){
  alert("Please wait. Page is loading!");
}
}
//https://www.sitepoint.com/guide-vanilla-ajax-without-jquery/
